# How-To

Create a keytab for your principle, the script will save keytab to your username.keytab
    ```./create_keytab.sh User.Name@MY.CORP```
 
Then make sure you update the path to where you put these scripts in the file local.kerberos.plist
    ```<string>~/.dotfiles/launchd/renew_kerberos_ticket.sh</string>```
 
Load the job
    ```./load_job.sh```
 
To test, make the loaded job run
    ```launchctl start local.kerberos```
 
and then run
    ```klist```
 
and you should see something like this:
 
```bash
Credentials cache: API:XXXXX
        Principal: User.Name@MY.CORP
 
  Issued                Expires               Principal
Jul  7 12:23:23 2019  Jul  7 22:23:23 2019  krbtgt/MY.CORP@MY.CORP
```
