#if ! is-macos -o ! is-executable ruby -o ! is-executable curl -o ! is-executable git; then
#  echo "Skipped: Homebrew (missing: ruby, curl and/or git)"
#  return
#fi

# ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

brew tap Goles/battery
brew tap derailed/k9s
brew tap derailed/popeye
brew tap InstantClientTap/instantclient
brew update
brew upgrade

# Install packages

apps=(
  ansible
  awscli
  bash
  bash-completion@2
  coreutils
  dockutil
  elixir
  erlang
  fasd
  fontconfig
  freetype
  gd
  gdbm
  gettext
  glib
  glide
  go
  graphviz
  gts
  highlight
  jq
  k9s
  kube-ps1
  kubectx
  kubernetes-cli
  kubernetes-helm
  libffi
  libidn2
  libpng
  libtiff
  libtool
  libunistring
  libyaml
  lua
  mackup
  mas
  mc
  netpbm
  npm
  nvm
  oniguruma
  openssl
  pcre
  pkg-config
  popeye
  prometheus
  psgrep
  python
  python@2
  rancher-cli
  readline
  shellcheck
  sphinx-doc
  sqlite
  telnet
  terraform
  thefuck
  unar
  webp
  wget
  wxmac
  xz
)

brew install ${apps[@]}

export DOTFILES_BREW_PREFIX_COREUTILS=`brew --prefix coreutils`
#set-config "DOTFILES_BREW_PREFIX_COREUTILS" "$DOTFILES_BREW_PREFIX_COREUTILS" "$DOTFILES_CACHE"

#ln -sfv "$DOTFILES_DIR/etc/mackup/.mackup.cfg" ~

# Need to download Oracle Instantclient from Oracle re Licence:
# http://www.oracle.com/technetwork/topics/intel-macsoft-096467.html.
# then follow move commandss
# brew install instantclient-basic
# brew install instantclient-sdk