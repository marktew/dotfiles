if ! is-macos -o ! is-executable brew; then
  echo "Skipped: MAS"
  return
fi

mas signin mark.tew@<replace_me>

# Install packages
# Xcode             497799835
# 1Password         1333542190 (out)
# Alfred            405843582 (out)
# Amphetamine       937984704
# AppCleaner        1013897218 (out)
# EasyFind          411673888
# OneDrive          823766827 (out)
# Pocket            568494494 (out)
# TextWrangler      404010395 (out)
# The Unarchiver    425424353
# Wunderlist        410628904
# Notability        736189492 (out)
# Termius           1176074088 (out)
# Slack             803453959 (out)

apps=(
  497799835
  937984704
  411673888
  425424353
  410628904
)

mas install "${apps[@]}"
